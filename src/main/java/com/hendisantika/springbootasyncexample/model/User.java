package com.hendisantika.springbootasyncexample.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-async-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/11/18
 * Time: 05.41
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@ToString
public class User {
    private String name;
    private String blog;

}
