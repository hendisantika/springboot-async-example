# Spring Boot Async Example

Run this project by this command : `mvn clean spring-boot:run`

```
2018-11-17 06:07:15.705  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : Started SpringbootAsyncExampleApplication in 1.874 seconds (JVM running for 6.531)
2018-11-17 06:07:15.720  INFO 22094 --- [        Async-3] c.h.s.service.GitHubLookupService        : Looking up Spring-Projects
2018-11-17 06:07:15.720  INFO 22094 --- [        Async-2] c.h.s.service.GitHubLookupService        : Looking up CloudFoundry
2018-11-17 06:07:15.720  INFO 22094 --- [        Async-1] c.h.s.service.GitHubLookupService        : Looking up PivotalSoftware
2018-11-17 06:07:15.720  INFO 22094 --- [        Async-4] c.h.s.service.GitHubLookupService        : Looking up hendisantika
2018-11-17 06:07:17.573  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : Elapsed time: 1865
2018-11-17 06:07:17.574  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : --> User(name=Pivotal Software, Inc., blog=http://pivotal.io)
2018-11-17 06:07:17.574  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : --> User(name=Cloud Foundry, blog=https://www.cloudfoundry.org/)
2018-11-17 06:07:17.574  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : --> User(name=Spring, blog=http://spring.io/projects)
2018-11-17 06:07:17.575  INFO 22094 --- [           main] c.h.s.SpringbootAsyncExampleApplication  : --> User(name=Hendi Santika, blog=https://www.facebook.com/hendisantika)

```